# What?
A tiny file upload/download server.

# Usage:

Server Standpoint:
```
fServ

or

fServ /path/you/want/to/serve
```

If started without arguments fServ will serve whatever the current local directory is, be careful about popping this up in the middle of your home directory unless you want someone to be able to download your .ssh directory or something.
When clients upload files to /fUpd they're injected into whatever directory fServ is configured for.
Directories are resolved as paths on the server, so if you have /tmp/something/cool.txt your client would visit http://hostname:8090/tmp/something/cool.txt.

Client standpoint:
```
To upload a file:
curl -F file=@file.whatever -F engima=xxx http://127.0.0.1:8090/fUpd

To download a file:
lynx http://127.0.0.1:8090
```

You don't need to use curl or lynx to make use of fServ, any HTTP browser will work fine for downloads. fServ expects the engima phrase inside of the boundaries of the multi-part-form upload.
