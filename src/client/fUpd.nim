import os, httpclient, mimetypes, strutils
#nim c -d:release fUpd.nim

proc fUpd(): string =
  if paramCount() < 3:
    return "Usage: fUpd x.x.x.x engima /path/to/file"

  var
    client = newHttpClient()
    data = newMultipartData()

  let
    mimes = newMimetypes()
    fserv = "http://" & paramStr(1) & ":8090/fUpd"
    code = paramStr(2)
    file = paramStr(3)

  data["enigma"] = $code
  data.addFiles({"file": $file}, mimeDb = mimes)
  client.postContent($fserv, multipart=data).strip()

echo fUpd()
