package main

import (
	"os"
	"io"
	"fmt"
	"bytes"
	"strings"
	"net/http"
	"path/filepath"
	"mime/multipart"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Usage: fUpd x.x.x.x enigma /path/to/file")
		os.Exit(1)
	}

	//Read in the file to post
	f, _ := os.Open(os.Args[3])
	defer f.Close()

	//Create a buffer/writer for our form
	b := &bytes.Buffer{}
	w := multipart.NewWriter(b)
	//Create a multipart form with the first field as engima, then write the engima string as its value
	p, _ :=	w.CreateFormField("enigma")
	io.Copy(p, strings.NewReader(os.Args[2]))
	//Create a file field and copy the file into it
	p, _ = w.CreateFormFile("file", filepath.Base(f.Name()))
	io.Copy(p, f)
	w.Close()

	//Create an HTTP POST Request with our form data
	r, _ := http.NewRequest("POST", "http://" + os.Args[1] + ":8090/fUpd", b)
	r.Header.Add("Content-Type", w.FormDataContentType())
	//Init http client, issue request, read response from fServ
	c := &http.Client{}
	rsp, err := c.Do(r)
	if err != nil {
		fmt.Println(err)
	} else {
		rb := &bytes.Buffer{}
		_, err := rb.ReadFrom(rsp.Body)
		if err != nil {
			fmt.Println(err)
		}
		rsp.Body.Close()
		fmt.Println(rb)
	}
}
