@ECHO off
SETLOCAL EnableDelayedExpansion
SET addr=%1
SET enigma=%2
SET file=%3
SET usage=Usage: fUpd.bat addr enigma file

IF '%addr%' == '' (
   GOTO :USAGE
   ) ELSE IF '%enigma%' == '' (
   	 GOTO :USAGE
	 ) ELSE IF '%file%' == '' (
	   GOTO :USAGE
	   ) ELSE (GOTO :fUpd)

:USAGE
ECHO %usage%
GOTO ABORT

:ABORT
EXIT /B 1

:fUpd
curl -F file=@%file% -F enigma=%enigma% http://%addr%:8090/fUpd
