import requests, sys

def fUpd():
    if len(sys.argv) < 3:
        print("Usage: fUpd x.x.x.x engima /path/to/file")
        sys.exit(1)

    fserv = "http://" + sys.argv[1] + ":8090/fUpd"
    code = sys.argv[2]
    f = sys.argv[3]

    req = requests.post(fserv, files={'file': open(f, 'rb')}, data={'enigma': code})
    print(req.text)

fUpd()
