#!/bin/sh
addr=$1
enigma=$2
file=$3

function fUpd() {
	curl -F file=@$file -F enigma=$enigma http://$addr:8090/fUpd
}

if [ -z $addr ] | [ -z $enigma ] | [ -z $file ]; then
	echo "Usage: fUpd.sh ip_addr enigma file"
	exit 1
else
	fUpd $addr $enigma $file
fi

