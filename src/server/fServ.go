package main

import (
	"io"
	"os"
	"net"
	"fmt"
	"log"
	"math/rand"
	"net/http"
)

//Generate random string of X size from range of runes
func enigma(n int) string {
    var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
    b := make([]rune, n)
    for i := range b {
        b[i] = letters[rand.Intn(len(letters))]
    }
    return string(b)
}

func getAddr() string {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)
	return localAddr.IP.String()
}

//Handle file upload via POST, validate via randomly generated string
func fUpd(vdir string, EnigmaPhrase string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("Upload initiated..")

		//32mb mem limit, anything greater is passed to tmpfs
		err := r.ParseMultipartForm(32 << 20)
		if err != nil {
			log.Println(err)
			http.Error(w, "Failed to parse multipart message..", http.StatusBadRequest)
			return
		}

		e := r.FormValue("enigma")

		if e != EnigmaPhrase {
			log.Println("Invalid Enigma phrase: " + e)
			http.Error(w, "Invalid enigma phrase..", http.StatusInternalServerError)
			return
		} else if e == EnigmaPhrase {
			f, h, err := r.FormFile("file")
			if err != nil {
				log.Println(err)
				http.Error(w, "Cannot read file..", http.StatusInternalServerError)
				return
			}
			defer f.Close()
			
			dst, err := os.Create(vdir + "/" + h.Filename)
			if err != nil {
				log.Println(err)
				http.Error(w, "Cannot create local file..", http.StatusInternalServerError)
				return
			}
			defer dst.Close()
			
			if _, err := io.Copy(dst, f); err != nil {
				log.Println(err)
				http.Error(w, "Cannot copy to contents to file..", http.StatusInternalServerError)
				return
			}

			consumption := "fUpd consumed: " + vdir + "/" + h.Filename
			w.Write([]byte(consumption))
			log.Println(consumption)
		}
	}
}
	
func main () {	
	var vdir = "."
	if len(os.Args[1:]) >= 1 {
		vdir = os.Args[1]
	}
	fs := http.Dir(vdir)
	phrase := enigma(8)
	laddr := getAddr()
	
	fmt.Printf("File Server started on port 8090 serving directory %s\n", vdir)
	fmt.Printf("Enigma Phrase: %s\n\n", phrase)

	fmt.Println("File Server Usage:")
	fmt.Printf("wget http://%s:8090/file\n", laddr)
	fmt.Printf("Invoke-WebRequest -Uri http://%s:8090/file -OutFile file\n\n", laddr)

	fmt.Println("/fUpd usage examples:")
	fmt.Printf("fUpd %s %s %s\n", laddr, phrase, "file")
	fmt.Printf("curl -F file=@file -F enigma=%s http://%s:8090/fUpd\n", phrase, laddr)

	//Serve files from specified dir, path from dir is path from fServ. (ie: ~/distrib turns into /distrib and a file in there is /distrib/file.name
	http.Handle("/", http.FileServer(fs))
	//Handle Posts to /fUpd for uploading
	http.HandleFunc("/fUpd", fUpd(vdir, phrase))
	http.ListenAndServe(":8090", nil)
}
