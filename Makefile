BINDIR ?= /usr/local/bin

compile:
	cd src/server && go build

compile-win:
	cd src/server && GOOS=windows GOARCH=amd64 go build

compile-plan9:
	cd src/server && GOOS=plan9 GOARCH=amd64 go build

compile-macos:
	cd src/server && GOOS=darwin GOARCH=amd64 go build

compile-client:
	cd src/client && nim c -d:release fUpd.nim

install-server:
	install -Dm 755 src/server/fServ $(BINDIR)/fServ

install-client:
	install -Dm 755 src/client/fUpd $(BINDIR)/fUpd
